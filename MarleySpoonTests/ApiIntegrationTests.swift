//
//  ApiIntegrationTests.swift
//  MarleySpoonTests
//
//  Created by Hassaan Fayyaz Ahmed on 15/08/2021.
//

import XCTest
import Foundation
@testable import MarleySpoon
@testable import Contentful

// Testing Contentful API Integration
class ApiIntegrationTests: XCTestCase {
    private let apiClient =  Client(spaceId: Constants.ApiKeys.spaceId.rawValue, accessToken: Constants.ApiKeys.accessToken.rawValue, contentTypeClasses: [Recipe.self, Chef.self, RecipeTag.self])

    // testing if the host and spaceId are properly set
    func testBasicApiRequestVerification() throws {
        let host: String = "cdn.contentful.com"
        let spaceId = Constants.ApiKeys.spaceId.rawValue
        let unwrappedHost = try XCTUnwrap(host)
        XCTAssertEqual(apiClient.host, unwrappedHost)
        XCTAssertEqual(apiClient.spaceId, spaceId)
    }
    
    // testing if the api response is properly received
    func testBasicApiResponseVerification() throws {
        var recipes: [Recipe]?
        let recipeExpectation = expectation(description: "Recipes")
        let query = QueryOn<Recipe>.where(contentTypeId: Recipe.contentTypeId)
        
         apiClient.fetchArray(of: Recipe.self, matching: query) { result in
            switch result {
            case .success(let entriesArrayResponse):
                recipes = entriesArrayResponse.items
                for item in entriesArrayResponse.items {
                    // Making sure that mandatory fields are always present
                    XCTAssertNotNil(item.title, "Missing title of recipe.")
                    XCTAssertNotNil(item.description, "Missing description of recipe.")
                }
                // Make sure we downloaded some data.
                XCTAssertNotNil(entriesArrayResponse, "No data was received.")
                recipeExpectation.fulfill()
                
            case .failure(_):
                XCTFail("Received error froms server")
            }
        }
        
        waitForExpectations(timeout: 10.0) { _ in
            XCTAssertNotNil(recipes, "No data received for recipes")
        }
    }
    
    // Testing when incorrect search query is used
    func testApiErrorCase() throws {
        let recipeExpectation = expectation(description: "Recipes")
        let query = QueryOn<Recipe>.where(contentTypeId: "IncorrectId")
        var isErrorReceived = false

        apiClient.fetchArray(of: Recipe.self, matching: query) { result in
           switch result {
           case .success(_):
            isErrorReceived = false
            recipeExpectation.fulfill()
           case .failure(_):
            isErrorReceived = true
            recipeExpectation.fulfill()
           }
       }
       waitForExpectations(timeout: 10.0) { _ in
           XCTAssertTrue(isErrorReceived, "Should have received error")
       }

    }
}
