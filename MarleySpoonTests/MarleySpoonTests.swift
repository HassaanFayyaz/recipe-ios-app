//
//  MarleySpoonTests.swift
//  MarleySpoonTests
//
//  Created by Hassaan Fayyaz Ahmed on 11/08/2021.
//

import XCTest
import Foundation
@testable import MarleySpoon
@testable import Contentful

class MarleySpoonTests: XCTestCase {

    private let apiClient = Client(spaceId: Constants.ApiKeys.spaceId.rawValue, accessToken: Constants.ApiKeys.accessToken.rawValue, contentTypeClasses: [Recipe.self, Chef.self, RecipeTag.self])

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        self.measure {
        }
    }
}
