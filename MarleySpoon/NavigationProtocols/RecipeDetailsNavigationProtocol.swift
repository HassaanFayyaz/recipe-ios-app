//
//  RecipeDetailsNavigationProtocol.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 14/08/2021.
//

import Foundation
import UIKit

protocol RecipeDetailsNavigationProtocol {
    @discardableResult
    func navigateToRecipeDetails(from sourceVC: UIViewController, with recipe: Recipe) -> Bool
}

extension RecipeDetailsNavigationProtocol {
    @discardableResult
    func navigateToRecipeDetails(from sourceVC: UIViewController, with recipe: Recipe) -> Bool {
        let mainSB = UIStoryboard.init(name: "Main", bundle: .main)
        guard let recipeDetailsVC = mainSB.instantiateViewController(identifier: "RecipeDetailsVC") as? RecipeDetailsVC else {
            return false
        }
        recipeDetailsVC.recipe = recipe
        if let navController = sourceVC.navigationController {
            navController.pushViewController(recipeDetailsVC, animated: true)
        } else {
            sourceVC.present(recipeDetailsVC, animated: true, completion: nil)
        }
        return false
    }

}
