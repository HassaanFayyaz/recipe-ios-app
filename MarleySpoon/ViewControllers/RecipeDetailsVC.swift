//
//  RecipeDetailsVC.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 14/08/2021.
//

import UIKit

class RecipeDetailsVC: UIViewController {

    // IB Outlests
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var calorieView: UIView!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var dishTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var chefNameLabel: UILabel!
    @IBOutlet weak var chefByLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!

    // Class vaiables
    /// The `Recipe` object that contains all the required information.
    var recipe: Recipe?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.populateData()
    }

    /// Initializing views and their presentaions
    private func setupViews() {
        self.dishImageView.layer.cornerRadius = 5
        self.calorieView.layer.cornerRadius = 5
    }

    /// Populating data in the views
    private func populateData() {
        guard let recipe = self.recipe else { return }

        self.dishImageView.sd_setImage(with: recipe.image?.url, completed: nil)
        // Setting up Title
        if let title = recipe.title { self.dishTitleLabel.text = title
        } else { self.dishTitleLabel.text = "" }

        // Setting up Calories View
        if let calories = recipe.caloriesText {
            self.caloriesLabel.text = calories
            self.calorieView.isHidden = false
        } else {
            self.calorieView.isHidden = true
        }

        // Chef Name
        if let chefName = recipe.chef?.name {
            self.chefNameLabel.text = chefName
            self.chefNameLabel.isHidden = false
            self.chefByLabel.isHidden = false
        } else {
            self.chefNameLabel.isHidden = true
            self.chefByLabel.isHidden = true
        }
        // Tags
        let tags = recipe.tags?.compactMap { $0.name?.capitalized(with: nil) }
        self.tagsLabel.text = tags?.joined(separator: ", ")

        // Recipe Description
        if let description = recipe.description {
            self.descriptionLabel.text = description
            self.descriptionLabel.isHidden = false
        } else {
            self.descriptionLabel.isHidden = true
        }
    }

}
