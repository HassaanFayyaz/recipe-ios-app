//
//  RecipeListVC.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 11/08/2021.
//

import UIKit
import Contentful
import MBProgressHUD

class RecipeListVC: UIViewController, RecipeDetailsNavigationProtocol {

    // IB Outlets
    @IBOutlet weak var recipeTableview: UITableView!

    // Class variables
    private let recipeListVM = RecipeListVM()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViewModel()
        self.setupViews()

    }

    /// Initializing the viewModel object associated with this VC
    private func setupViewModel() {
        self.recipeListVM.onSuccessReceivingRecipeData = { [weak self] in
            guard let self = self else { return }
            self.recipeTableview.isHidden = false
            self.recipeTableview.reloadData()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        self.recipeListVM.onErrorReceivingRecipeData = { [weak self] in
            guard let self = self else { return }
            self.recipeTableview.isHidden = true
            MBProgressHUD.hide(for: self.view, animated: true)
        }

        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.recipeListVM.fetchRecipeDataFromServer() // Making the call to fetch data
    }

    /// Initializing views and their presentaions
    private func setupViews() {
        self.recipeTableview.rowHeight = UITableView.automaticDimension
        self.recipeTableview.estimatedRowHeight = 120.0
        self.recipeTableview.tableFooterView = .init() // removing empty rows from end
        self.recipeTableview.isHidden = true
    }
}

// MARK: Tableview Delegate, Datasource
extension RecipeListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipeListVM.numberOfRecipes
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "All Time Favorite Recipes"
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let recipeCell = tableView.dequeueReusableCell(withIdentifier: "RecipeTVCell", for: indexPath) as? RecipeTVCell else {
            fatalError("The recipe cell has to be available in tableview")
        }
        if let recipe = self.recipeListVM.getRecipe(at: indexPath.item) {
            recipeCell.setCellData(with: recipe)
        }
        return recipeCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if let recipe = self.recipeListVM.getRecipe(at: indexPath.item) {
            self.navigateToRecipeDetails(from: self, with: recipe)
        }
    }
}
