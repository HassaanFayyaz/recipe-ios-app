//
//  RecipeTag.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 15/08/2021.
//

import Foundation
import Contentful

class RecipeTag: EntryDecodable, Resource, FieldKeysQueryable {

    static let contentTypeId = "tag"

    let sys: Sys
    /// Name of the chef
    let name: String?

    required init(from decoder: Decoder) throws {
        sys = try decoder.sys()
        let fields = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        name = try fields.decodeIfPresent(String.self, forKey: .name)
    }

    enum FieldKeys: String, CodingKey {
        case name
    }
}
