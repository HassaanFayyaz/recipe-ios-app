//
//  Recipe.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 14/08/2021.
//

import Foundation
import Contentful

final class Recipe: EntryDecodable, FieldKeysQueryable {
    static var contentTypeId: String = "recipe"

    // FlatResource members.
    let id: String
    let localeCode: String?
    let updatedAt: Date?
    let createdAt: Date?

    /// Name of the `recipe`
    let title: String?
    /// The calorie count of this `recipe`
    let calories: Int?
    /// Details about this `recipe`
    let description: String?
    /// image of this `recipe`
    var image: Asset?

    var chef: Chef?
    var tags: [RecipeTag]?

    public required init(from decoder: Decoder) throws {
        let sys         = try decoder.sys()
        id              = sys.id
        localeCode      = sys.locale
        updatedAt       = sys.updatedAt
        createdAt       = sys.createdAt

        let fields      = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        self.title      = try fields.decodeIfPresent(String.self, forKey: .title)
        self.calories   = try fields.decodeIfPresent(Int.self, forKey: .calories)
        self.description = try fields.decodeIfPresent(String.self, forKey: .description)

        try fields.resolveLink(forKey: .image, decoder: decoder) { [weak self] image in
            self?.image = image as? Asset
        }
        try fields.resolveLink(forKey: .chef, decoder: decoder) { [weak self] chef in
            self?.chef = chef as? Chef
        }
        try fields.resolveLinksArray(forKey: .tags, decoder: decoder) { [weak self] tags in
            self?.tags = tags as? [RecipeTag]
        }
    }

    enum FieldKeys: String, CodingKey {
        case title, calories, description, chef, tags
        case image = "photo"
    }

    /// The value to be shown to the user. Returns nil when the calorie value is incorrect
    var caloriesText: String? {
        guard let calorie = self.calories, calorie >= 0 else { return nil }
        if calorie == 0 {
            return "0 Calorie"
        } else {
            return "\(calorie) Calories"
        }
    }
}
