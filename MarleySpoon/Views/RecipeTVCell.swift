//
//  RecipeTVCell.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 14/08/2021.
//

import UIKit
import SDWebImage

class RecipeTVCell: UITableViewCell {

    // IB Outlets
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var dishTitleLabel: UILabel!
    @IBOutlet weak var dishCaloriesLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.dishImageView.layer.cornerRadius = 5

        let highlightedView = UIView.init(frame: self.frame)
        highlightedView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.selectedBackgroundView = highlightedView

    }

    /// Update the information to be displayed in a `Recipe` table cell
    func setCellData(with recipe: Recipe) {

        if let title = recipe.title { self.dishTitleLabel.text = title
        } else { self.dishTitleLabel.text = "" }

        self.dishCaloriesLabel.text = recipe.caloriesText

        if let imageUrl = recipe.image?.file?.url {
            self.dishImageView.sd_setImage(with: imageUrl, placeholderImage: nil, options: .init(), context: nil)
        }
    }

}
