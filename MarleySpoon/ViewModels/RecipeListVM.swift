//
//  RecipeListVM.swift
//  MarleySpoon
//
//  Created by Hassaan Fayyaz Ahmed on 13/08/2021.
//

import Foundation
import Contentful

class RecipeListVM {

    private let apiClient =  Client(spaceId: Constants.ApiKeys.spaceId.rawValue, accessToken: Constants.ApiKeys.accessToken.rawValue, contentTypeClasses: [Recipe.self, Chef.self, RecipeTag.self])

    private var recipeList: [Recipe] = []

    /// called when the recipe data is received from server
    var onSuccessReceivingRecipeData: (() -> Void)?
    /// called when we get an error in receiving recipe data
    var onErrorReceivingRecipeData: (() -> Void)?

    var numberOfRecipes: Int { self.recipeList.count }

    func getRecipe(at index: Int) -> Recipe? {
        guard index >= 0, index < numberOfRecipes else { return nil }
        return self.recipeList[index]
    }

    /// This is responsible for fetching data from server
    func fetchRecipeDataFromServer() {
        let query = QueryOn<Recipe>.where(contentTypeId: Recipe.contentTypeId)
        self.apiClient.fetchArray(of: Recipe.self, matching: query) { [weak self] (result ) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch result {
                case .success(let entriesArrayResponse):
                    self.recipeList = entriesArrayResponse.items

                    self.onSuccessReceivingRecipeData?()
                case .failure(let error):
                    print("Oh no something went wrong: \(error)")
                    self.recipeList.removeAll()
                    self.onErrorReceivingRecipeData?()
                }

            }
        }

    }

}
