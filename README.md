# Recipe App #

A simple app written in Swift that uses contentful's sdk to fetch data from contentful. The app has only 2 screen.

1. A list view of recipes
2. Details of a recipe

### What does the app support/contain? ###

* The app supports Portrait and Landscape modes
* The app supports Light and Dark modes introduced in iOS 13.
* The app uses MVVM architecture pattern
* [SwiftLint](https://github.com/realm/SwiftLint) is used to standardize coding style
* There are 3 different SDKs used via Swift Package Manager.

1. [ContentFul](https://www.contentful.com/developers/docs/ios/tutorials/using-delivery-api-with-swift/) to fetch data from ContentFul
2. [SDWebImage](https://github.com/SDWebImage/SDWebImage) to download and cache images from server
3. [MBProgressHUD](https://github.com/jdg/MBProgressHUD) to show processing indication  

### How to run the project? ###

1. Clone the project
2. Open the `MarleySpoon.xcodeproj` file
3. Let the Swift Package Manager install dependencies
4. Run the project.
5. There are a few TestCases written as well
6. Optionally, [SwiftLint](https://github.com/realm/SwiftLint) should also be installed

### What can be improved ###

* The test coverage is less than 50. We can improve test cases.
* In Git, there is only single branch `master` is used. We can use the proper [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html), if needed
* There is no CI used at the moment. We can also use it to streamline our system.
* Currently, the network calls are in ViewModel. As the code grows, we can consider moving network calls to a separate NetworkLayer.
* There is some room for improvement in the UI.

### Screenshots ##

* Here are the screens for [ListView 1](https://drive.google.com/file/d/1Xw8vRfjCCsxsQpOa5qOvCbHDlEBUL9yB/view?usp=sharing), [DetailsView 1](https://drive.google.com/file/d/1rdHRvs9y0gW9zMjC8HV7x8ZtgYCKKhmW/view?usp=sharing)

* Here are the screens for [ListView 2](https://drive.google.com/file/d/1r30Fb499duaKGUdkSOQ42dBv8qZWUriY/view?usp=sharing) and [DetailsView 2](https://drive.google.com/file/d/1ecTajzC0mUvyP1LSnq5e3igTwqtOaQ0b/view?usp=sharing)
